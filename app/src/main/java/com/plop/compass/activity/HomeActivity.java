package com.plop.compass.activity;

import android.Manifest.permission;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding3.widget.RxTextView;
import com.plop.compass.R;
import com.plop.compass.model.ApplicationCompass;
import com.plop.compass.model.CompassSensorData;
import com.plop.compass.presenter.HomePresenter;
import com.plop.compass.util.TextUtil;
import com.plop.compass.view.HomeView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

import static com.plop.compass.util.Constants.MIN_DISTANCE;
import static com.plop.compass.util.Constants.MIN_TIME_MS;
import static com.plop.compass.util.Constants.REQUEST_LOCATION;

@RuntimePermissions
public class HomeActivity extends AppCompatActivity implements HomeView, SensorEventListener, LocationListener {

    @BindView(R.id.compass)
    ApplicationCompass compassView;
    @BindView(R.id.tv_location)
    TextView currentLocationView;
    @BindView(R.id.destination_direction)
    TextView navigationTip;
    @BindView(R.id.latitude_input)
    EditText destinationLatEt;
    @BindView(R.id.longitude_input)
    EditText destinationLngEt;
    @BindView(R.id.btn_text)
    TextView button;

    private HomePresenter presenter;

    private Unbinder unbinder;
    private SensorManager sensorManager;
    private Disposable inputListenerDisposable;

    private Sensor accelerometer;
    private Sensor magnetometer;
    private CompassSensorData csd = new CompassSensorData();
    private Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        unbinder = ButterKnife.bind(this);
        presenter = new HomePresenter(this, compassView);

        askPermission();
        setupSensorManager();
        setupLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerSensor();
        initLatLongInputListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterSensor();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        inputListenerDisposable.dispose();
        unbinder.unbind();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        presenter.onSensorChanged(sensorEvent, accelerometer, magnetometer, csd);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //do nothing
    }

    @Override
    public void onLocationChanged(Location location) {
        setupLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        //do nothing
    }

    @Override
    public void onProviderEnabled(String s) {
        //do nothing
    }

    @Override
    public void onProviderDisabled(String s) {
        //do nothing
    }

    @SuppressWarnings("MissingPermission")
    @NeedsPermission({permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION})
    public void askLocation() {

    }

    @OnPermissionDenied({permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION})
    void showDeniedForLocation() {
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomeActivityPermissionsDispatcher.onRequestPermissionsResult(this,
                requestCode, grantResults);
    }

    @Override
    public void updateCompassStatus(Sensor accelerometer, Sensor magnetometer, CompassSensorData csd,
                                    float azimuthInDegrees) {
        this.accelerometer = accelerometer;
        this.magnetometer = magnetometer;
        this.csd = csd;
        presenter.rotateCompass(azimuthInDegrees);
    }

    @Override
    public void displayCurrentLocationView(Location location) {
        currentLocationView.setText(TextUtil.convertLocationToString(location.getLatitude(),
                location.getLongitude()));
    }

    @OnClick(R.id.btn_text)
    public void onNavigateBtnClicked() {
        if (location != null) {
            String direction = TextUtil.indicateDirection(location.getLatitude(),
                    location.getLongitude(), Double.valueOf(destinationLatEt.getText().toString()),
                    Double.valueOf(destinationLngEt.getText().toString()));

            navigationTip.setText(String.format(getString(R.string.destination_direction), direction));
            navigationTip.setVisibility(View.VISIBLE);
        } else {
            displayMessage(getString(R.string.current_location_unavailable));
        }
    }

    private void askPermission() {
        final boolean accessFineLocation =
                ActivityCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
        final boolean accessCoarseLocation = ActivityCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED;

        if (accessFineLocation && accessCoarseLocation) {
            HomeActivityPermissionsDispatcher.askLocationWithPermissionCheck(this);
        }
    }

    private void setupLocation() {
        if ((ActivityCompat.checkSelfPermission(this,
                permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(this,
                permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(this,
                    new String[]{permission.ACCESS_COARSE_LOCATION,
                            permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_MS,
                    MIN_DISTANCE, this);

            if (location != null && currentLocationView != null) {
                this.location = location;
                presenter.updateCurrentLocationView(location);
            } else {
                displayMessage(getString(R.string.location_unavailable));
            }
        }
    }

    private void initLatLongInputListener() {
        Observable<Boolean> latValidation = RxTextView.textChanges(destinationLatEt)
                .map(str -> !TextUtils.isEmpty(str.toString().trim()));
        Observable<Boolean> lngValidation = RxTextView.textChanges(destinationLngEt)
                .map(str -> !TextUtils.isEmpty(str.toString().trim()));
        inputListenerDisposable = Observable.combineLatest(latValidation, lngValidation,
                (isLatInputFilled, isLngInputFilled) -> isLatInputFilled && isLngInputFilled)
                .subscribe(this::toggleButtonState);
    }

    private void toggleButtonState(Boolean isValid) {
        button.setEnabled(isValid);
    }

    private void setupSensorManager() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    private void registerSensor() {
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    private void unregisterSensor() {
        sensorManager.unregisterListener(this);
    }

    private void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
