package com.plop.compass.view;

import android.hardware.Sensor;
import android.location.Location;

import com.plop.compass.model.CompassSensorData;

public interface HomeView {
    void updateCompassStatus(Sensor accelerometer, Sensor magnetometer, CompassSensorData csd,
                             float azimuthInDegrees);

    void displayCurrentLocationView(Location location);
}
