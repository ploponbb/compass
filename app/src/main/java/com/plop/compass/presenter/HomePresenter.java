package com.plop.compass.presenter;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;

import com.plop.compass.model.ApplicationCompass;
import com.plop.compass.model.CompassSensorData;
import com.plop.compass.view.HomeView;

public class HomePresenter {

    private HomeView view;
    private ApplicationCompass appCompass;

    public HomePresenter(HomeView view, ApplicationCompass appCompass) {
        this.view = view;
        this.appCompass = appCompass;
    }

    public void updateCurrentLocationView(Location location) {
        view.displayCurrentLocationView(location);
    }

    public void rotateCompass(float azimuthInDegrees) {
        appCompass.rotate(azimuthInDegrees);
        appCompass.setCurrentDegree(-azimuthInDegrees);
    }

    public void onSensorChanged(SensorEvent sensorEvent, Sensor accelerometer, Sensor magnetometer,
                                CompassSensorData csd) {
        float azimuthInDegrees = 0f;

        if (sensorEvent.sensor == accelerometer) {
            System.arraycopy(sensorEvent.values, 0, csd.getLastAccelerometer(), 0, sensorEvent.values.length);
            csd.setLastAccelerometerSet(true);
        } else if (sensorEvent.sensor == magnetometer) {
            System.arraycopy(sensorEvent.values, 0, csd.getLastMagnetometer(), 0, sensorEvent.values.length);
            csd.setLastMagnetometerSet(true);
        }

        if (csd.isLastAccelerometerSet() && csd.isLastMagnetometerSet()) {
            SensorManager.getRotationMatrix(csd.getRotationMatrix(), null,
                    csd.getLastAccelerometer(), csd.getLastMagnetometer());
            SensorManager.getOrientation(csd.getRotationMatrix(), csd.getOrientation());

            azimuthInDegrees = getAzimuthInDegrees(csd.getOrientation());
        }
        view.updateCompassStatus(accelerometer, magnetometer, csd, azimuthInDegrees);
    }

    private float getAzimuthInDegrees(float[] orientation) {
        float azimuthInRadians = orientation[0];
        return (float) Math.toDegrees(azimuthInRadians);
    }
}
