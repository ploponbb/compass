package com.plop.compass.util;

public class Constants {
    public static final int REQUEST_LOCATION = 2;
    public static final float PIVOT_X_VALUE = 0.5f;
    public static final float PIVOT_Y_VALUE = 0.5f;
    public static final long ANIMATION_TIME = 210;
    public static final long MIN_TIME_MS = 1000;
    public static final float MIN_DISTANCE = 0F;
}
