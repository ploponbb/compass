package com.plop.compass.util;

import android.location.Location;

public class TextUtil {

    public static String convertLocationToString(Double latitude, Double longitude) {
        StringBuilder builder = new StringBuilder();

        if (latitude < 0) builder.append("S ");
        else builder.append("N ");

        String latDegree = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS);
        String[] splittedLat = latDegree.split((":"));
        builder.append(splittedLat[0])
                .append("° ")
                .append(splittedLat[1])
                .append("\' ")
                .append(splittedLat[2].substring(0, 2))
                .append("\"").append("\n");

        if (longitude < 0) builder.append("W ");
        else builder.append("E ");

        String longDegree = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS);
        String[] splittedLong = longDegree.split((":"));
        builder.append(splittedLong[0]);
        builder.append("° ")
                .append(splittedLong[1])
                .append("\' ")
                .append(splittedLong[2].substring(0, 2))
                .append("\"");
        return builder.toString();
    }

    public static String indicateDirection(Double srcLatitude, Double srcLongitude, Double destLatitude,
                                     Double destLongitude) {
        StringBuilder directionBuilder = new StringBuilder();
        if (destLongitude > srcLongitude) {
            directionBuilder.append("N");
        } else {
            directionBuilder.append("S");
        }

        if (destLatitude > srcLatitude) {
            directionBuilder.append("E");
        } else {
            directionBuilder.append("W");
        }

        return directionBuilder.toString();
    }
}
