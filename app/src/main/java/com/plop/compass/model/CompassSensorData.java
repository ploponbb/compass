package com.plop.compass.model;

public class CompassSensorData {

    private float[] lastAccelerometer = new float[3];
    private float[] lastMagnetometer = new float[3];
    private boolean lastAccelerometerSet = false;
    private boolean lastMagnetometerSet = false;

    private float[] rotationMatrix = new float[9];
    private float[] orientation = new float[3];

    public CompassSensorData() {

    }

    public float[] getLastAccelerometer() {
        return lastAccelerometer;
    }

    public void setLastAccelerometer(float[] lastAccelerometer) {
        this.lastAccelerometer = lastAccelerometer;
    }

    public float[] getLastMagnetometer() {
        return lastMagnetometer;
    }

    public void setLastMagnetometer(float[] lastMagnetometer) {
        this.lastMagnetometer = lastMagnetometer;
    }

    public boolean isLastAccelerometerSet() {
        return lastAccelerometerSet;
    }

    public void setLastAccelerometerSet(boolean lastAccelerometerSet) {
        this.lastAccelerometerSet = lastAccelerometerSet;
    }

    public boolean isLastMagnetometerSet() {
        return lastMagnetometerSet;
    }

    public void setLastMagnetometerSet(boolean lastMagnetometerSet) {
        this.lastMagnetometerSet = lastMagnetometerSet;
    }

    public float[] getRotationMatrix() {
        return rotationMatrix;
    }

    public void setRotationMatrix(float[] rotationMatrix) {
        this.rotationMatrix = rotationMatrix;
    }

    public float[] getOrientation() {
        return orientation;
    }

    public void setOrientation(float[] orientation) {
        this.orientation = orientation;
    }
}
