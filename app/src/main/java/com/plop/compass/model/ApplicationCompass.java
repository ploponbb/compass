package com.plop.compass.model;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.plop.compass.R;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.plop.compass.util.Constants.ANIMATION_TIME;
import static com.plop.compass.util.Constants.PIVOT_X_VALUE;
import static com.plop.compass.util.Constants.PIVOT_Y_VALUE;

public class ApplicationCompass extends FrameLayout {

    @BindView(R.id.compass_view)
    ImageView compassView;

    private float currentDegree = 0f;
    private Unbinder unbinder;

    public ApplicationCompass(Context context) {
        super(context);
        inflateView();
    }

    public ApplicationCompass(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView();
    }

    public ApplicationCompass(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateView();

    }

    private void inflateView() {
        inflate(getContext(), R.layout.view_compass, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbinder.unbind();
    }

    public void setCurrentDegree(float currentDegree) {
        this.currentDegree = currentDegree;
    }

    public void rotate(float azimuthInDegrees) {
        RotateAnimation rotateAnimation = new RotateAnimation(
                currentDegree,
                -azimuthInDegrees,
                Animation.RELATIVE_TO_SELF, PIVOT_X_VALUE,
                Animation.RELATIVE_TO_SELF, PIVOT_Y_VALUE);
        rotateAnimation.setDuration(ANIMATION_TIME);
        rotateAnimation.setFillAfter(true);
        compassView.startAnimation(rotateAnimation);
    }
}
